# Tabela de Conteúdo
* [Resultados do Projeto](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/#resultados-do-projeto)
    * [Qiskit](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/#qiskit)
    * [QuISP](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/#quisp)
    * [NetSquid](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/#netsquid)
* [Material Complementar](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/#material-complementar)
    * [Minicurso](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/#minicurso)
    * [Virtual Exchange Seminars](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/#virtual-exchange-seminars)

# Resultados do Projeto
Este repositório armazena todo o conteúdo produzido pela pesquisa sobre Protocolos de Roteamento Quântico. Isso inclui, circuitos desenvolvidos em Python no Qiskit, simulações de rede em NetSquid e outros simuladores de redes quânticas, além disso, outros materiais como apresentações sobre o tema.
Outrossim, a página oficial do projeto conta com esses e outros conteúdos, e pode ser visitada em [quantumnet.gercom.ufpa.br](https://quantumnet.gercom.ufpa.br).

### Qiskit
O [Qiskit](https://qiskit.org/) é um framework Open-Source da linguagem Python para criação de circuitos quânticos, produzidos a partir de algum algoritmo, que podem ser executados em um computador quântico real, ou em um simulador. Apesar do Qiskit ter diferentes aplicações, os circuitos aqui criados são voltados essencialmente para o tema da pesquisa.

* O diretório [Qiskit_Network](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/Qiskt_Network) neste repositório guarda os códigos desenvolvidos.
* Para uma introdução à manipulação mais detalhada da tecnologia, veja os [Primeiros Passos](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/blob/main/Qiskt_Network/Primeiros%20Passos.md).

### QuISP
O Quatum Internet Simulation Package [(QuISP)](https://aqua.sfc.wide.ad.jp/quisp_website/) é um simulador orientado a eventos de redes de repetidores quânticos. O foco está no design de protocolos e no comportamento emergente de redes complexas e heterogêneas em grande escala, mantendo a camada física o mais realista possível. 

* As simulações feitas pelo projeto podem ser acessadas no diretório [QuISP_Topologias](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/QuISP_Topologias).

### NetSquid
O simulador Net work Spara Quantum Information using Discrete events [(NetSquid)](https://netsquid.org/) é uma ferramenta de software para a modelagem e simulação de redes quânticas escaláveis desenvolvidas na QuTech. O código é escrito em Python. Esta ferramenta não é Open-Source, mas é amplamente utilizado na área.

* As simulações no NetSquid feitas pelo projeto podem ser acessadas no diretório [NetSquid_Topologias](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/Nestquid_Topologias).

# Material Complementar
Além do conteúdo prático, há também materiais didáticos. A maior parte deles é proveniente de apresentações, feitas entre o grupo participante da pesquisa, ou em alguns eventos.

### Minicurso


### Virtual Exchange Seminars


### ...