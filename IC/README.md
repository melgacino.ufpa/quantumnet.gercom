# Iniciação Científica

Nesse diretório estão os códigos e materiais produzidos durante o Programa de Iniciação Científica de Arthur Henrique Azevedo Pimentel pela Universidade Federal do Pará, que teve início em 08/2022.

# Qiskit

Qiskit é um framework em Python para a construção de circuitos de computadores quânticos. Esses circuitos tem diversas aplicações, dependendo, claro, do intuito de quem o constrói. A utilização do Qiskit contribui com o entendimento do alicerce da computação quântica e como funcionam seus algoritmos. 

# QuNetSim

O QuNetSim é um simulador de redes quânticas. Também é escrito em Python.
...
