# QuNetSim

Nsse diretório você vai encontrar os códigos de exemplo e tutoriais para o QuNetSim.
Os códigos estão escritos com Python, mas podem ser adaptados para outros formatos, como Jupyter.

QuNetSim (Quantum Network Simulator) é um framework de simulação baseado em Python para simulações de redes quânticas. O uso pretendido é que se possa desenvolver e testar aplicações e protocolos projetados para redes quânticas na rede e camada de aplicação que podem transmitir e armazenar informações quânticas.

Como mencionado anteriormente, esse simulador é um framework do Python, além disso, o QuNetSim funciona em Linux, Mac OS X e Windows.

# Instalação

É recomendável a utilização de um ambiente virtual. Para instalar o QuNetSim, pode-se utilizar o pip da seguinte forma:
```
pip install qunetsim
```
Caso haja problemas com essa forma de instalar ou você preferir de outra maneira, é possível fazer a instalaçãpo clonando o repositório oficial. Para mais detalhes, [clique aqui](https://tqsd.github.io/QuNetSim/install.html).

Após a instalação, importe a biblioteca `qunetsim` de acordo com suas necessidades.

# Aprendendo o Simulador
1. Leia a [documentação](https://tqsd.github.io/QuNetSim/) do Simulador;
2. Teste os exemplos dispponíveis do [gitlab](https://gitlab.com/gercom-ufpa/quantumnet.gercom/-/tree/main/Turma%20Novas%20Tecnologias%20para%20Internet%20do%20Futuro/QuNetSim)
3. Leia a [visão geral do design](https://tqsd.github.io/QuNetSim/intro.html) para entender como o qunetsim foi escrito.
4. Confira os [exemplos da documentação](https://tqsd.github.io/QuNetSim/examples.html) para a explicação dos códigos.

# Links úteis:
[Repositório QuNetSim](https://github.com/tqsd/QuNetSim)<br>
[Como instalar o pip no Windows](https://neps.academy/br/blog/como-instalar-pip:-o-gerenciador-de-pacotes-do-python-no-windows)

### Nota:
Com o passar do tempo, a biblioteca do `QuNetSim` recebeu atualizações, mas a documentação não acompanhou as novidades. Por tanto, algumas funções estão escritas de uma forma desatualizada. Por esse e algum outro motivo, em alguns computadores a função `get_data_qubit()` funciona normalmente, em outros deve ser trocada por somente `get_qubit()`. Caso encontre problemas com elas, troque uma pela outra. Não houve nenhum outro erro desse tipo durante a execução dos códigos com outras funções, no entanto, caso encontre, tente atuaizar a biblioteca ou o `pip` (`pip3 install --upgrade pip`). Caso o erro persista, consulte a estrutura do QuNetSim, recomendo fazer isso com a IDE Visual Studio Code, basta clicar em cima das funções segurando o `Ctrl`, mas você é livre para escolher outras formas.

